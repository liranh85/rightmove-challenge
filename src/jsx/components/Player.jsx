'use strict'

import React from 'react';

class Player extends React.Component {
    
    render() {
        return (
            <div>
                <p>{this.props.name}: <span>{this.props.total}</span></p>
            </div>
        )
    }
}

export default Player;