'use strict'

import React from 'react';
import Player from '../components/Player.jsx';

class GameplayScreen extends React.Component {
    constructor(props) {
        super(props);
        
        this.state = {
            participants: {
                dealer: {
                    total: 0,
                    upperCard: null
                },
                player: {
                    total: 0
                }
            },
            gameStarted: false,
            gameLabels: ['Start game', 'Surrender'],
            finalDeal: false,
            winner: null,
            console: 'Click "Start game" when you\'re ready.'
        };

        this.data = {
            constants: {
                DEALER: 'dealer',
                PLAYER: 'player',
                PUSH: 'push'
            }
        };

        this.drawRandomCard = this.drawRandomCard.bind(this);
        this.dealCard = this.dealCard.bind(this);
        this.resetGameData = this.resetGameData.bind(this);
        this.startGame = this.startGame.bind(this);
        this.endGame = this.endGame.bind(this);
        this.clickStartOrEndGameHandler = this.clickStartOrEndGameHandler.bind(this);
        this.clickHitHandler = this.clickHitHandler.bind(this);
        this.finalDeal = this.finalDeal.bind(this);
        this.clickStandHandler = this.clickStandHandler.bind(this);
    }

    drawRandomCard() {
        const min = 1;
        const max = 11;
        return Math.floor(Math.random() * (max - min + 1)) + min;
    }

    dealCard(participant, dealerUpperCard = false) {
        if(!this.state.participants.hasOwnProperty(participant)) {
            console.error('No such participant: ', participant);
            return false;
        }
        const randomCard = this.drawRandomCard();
        const participants = this.state.participants;
        participants[participant].total += randomCard;
        if(participant === this.data.constants.DEALER && dealerUpperCard) {
            participants[this.data.constants.DEALER].upperCard = randomCard;
        }
        this.setState({participants});
        return randomCard;
    }

    resetGameData() {
        const participants = this.state.participants;
        participants.dealer.total = 0;
        participants.player.total = 0;
        this.setState({
            participants,
            finalDeal: false,
            winner: null
        });
    }

    startGame() {
        this.setState({gameStarted: true});
        this.resetGameData();
        const dealerCard1 = this.dealCard(this.data.constants.DEALER);
        const dealerUpperCard = true;
        const dealerCard2 = this.dealCard(this.data.constants.DEALER, dealerUpperCard);
        if(this.state.participants.dealer.total === 21) {
            this.setState({winner: this.data.constants.DEALER, console: 'The dealer got 21. You lose.'})
            this.endGame();
        }
        else {
            const playerCard1 = this.dealCard(this.data.constants.PLAYER);
            const playerCard2 = this.dealCard(this.data.constants.PLAYER);
            this.setState({console: `Game started. Dealer's upper card is ${dealerCard2}. Player got  ${playerCard1} and ${playerCard2}.`});
        }
    }

    endGame() {
        this.setState({gameStarted: false});
        if(this.state.finalDeal) {
            clearInterval(this.data.dealDealer);
        }
    }

    clickStartOrEndGameHandler() {
        if(!this.state.gameStarted) {
            this.startGame();
        }
        else {
            this.endGame();
            this.setState({console: 'You surrendered. You lose.'});
        }
    }
    
    clickHitHandler() {
        if(!this.state.gameStarted) {
            return;
        }
        const randomCard = this.drawRandomCard();
        const participants = this.state.participants;
        participants.player.total += randomCard;
        this.setState({participants, console: `You were dealt a ${randomCard}`});
        if(this.state.participants.player.total > 21) {
            this.setState({winner: this.data.constants.DEALER, console: `You busted. You lose. Your last card was ${randomCard}, which took you to a total of ${this.state.participants.player.total}`});
            this.endGame();
        }
    }

    finalDeal() {
        return new Promise((resolve, reject) => {
            const intervalLength = 1000;
            this.data.dealDealer = setInterval(() => {
                if(this.state.participants.dealer.total >= 17) {
                    clearInterval(this.data.dealDealer);
                    setTimeout(() => {
                        resolve();
                    }, intervalLength);
                }
                else {
                    const dealerCard = this.dealCard(this.data.constants.DEALER);
                    this.setState({console: `Dealer got ${dealerCard}.`});
                }
            }, intervalLength);
        });
    }
    
    clickStandHandler() {
        this.setState({finalDeal: true});
        this.finalDeal().then(() => {
            const dealerTotal = this.state.participants.dealer.total;
            const playerTotal = this.state.participants.player.total;
            this.endGame();

            if(dealerTotal > 21) {
                this.setState({winner: this.data.constants.PLAYER, console: 'Dealer busted. You win!'});
            }
            else if(playerTotal > dealerTotal) {
                this.setState({winner: this.data.constants.PLAYER, console: 'You beat the dealer. You win!'});
            }
            else if(playerTotal === dealerTotal) {
                this.setState({winner: this.data.constants.PUSH, console: 'Push. No one wins.'});
            }
            else {
                this.setState({winner: this.data.constants.DEALER, console: 'The dealer beat you. You lose.'});
            }
        });
    }

    render() {
        const gameplayButtons = (
            <div>
                <button onClick={this.clickHitHandler} className="actionBtn hitBtn">Hit</button>
                <button onClick={this.clickStandHandler} className="actionBtn standBtn">Stand</button>
            </div>
        );
        const dealerData = this.state.participants.dealer;
        const dealerTotal = (this.state.finalDeal || !this.state.gameStarted) ? dealerData.total : '? and ' + dealerData.upperCard;
        
        return (
            <div className="screen-container gameplay-screen">
                <h1>Blackjack</h1>
                <Player
                    name="Dealer"
                    total={dealerTotal}
                />
                <button onClick={this.clickStartOrEndGameHandler} className="actionBtn startOrEndGameBtn">{this.state.gameLabels[this.state.gameStarted ? 1 : 0]}</button>
                {(this.state.gameStarted && !this.state.finalDeal) ? gameplayButtons : ''}
                <Player
                    name="Player"
                    total={this.state.participants.player.total}
                />
                <p className="console">{this.state.console}</p>
            </div>
        )
    }
}

export default GameplayScreen;