import React from "react";
import appStates from './constants/appStates.jsx'
import GameplayScreen from './screens/GameplayScreen.jsx';

class App extends React.Component {
	constructor(props) {
		super(props);

		this.state = {
			currentScreen: appStates.GAMEPLAY_SCREEN
		};
	}

	render() {
		const appPartials = {
			[appStates.GAMEPLAY_SCREEN]:
				<GameplayScreen />
		}

		let currentScreenComponent = appPartials[this.state.currentScreen];

		return (
			<div>
				{currentScreenComponent}
			</div>
		);
	}
}

export default App;