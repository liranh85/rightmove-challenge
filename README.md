# Installation: #
## From the terminal: ##
- npm i
- gulp (this will spin up a virtual server, and watch for changes in the code)

## In your browser: ##
- Navigate to localhost:8080

# Notes: #
# Implemented: #
- Fully working basic game functionality (hit and stand), barring the use of cards and the logic of the Ace

# Would have implemented if spent more time: #
- Game features:
    - Using cards
    - Ace is 1 or 11 and its associated logic.
    - Betting money
    - Split
    - Insurance
    - Side bets
    - Multiple players

- Design and player experience:
    - Better UI and UX
    - Blackjack table layout and CSS animations